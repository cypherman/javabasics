import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh09N107;

public class TaskCh09N107Test {
    @Test
    public void testSwitchAWithO() {
        Assert.assertEquals("Moancoke", new TaskCh09N107().switchAWithO("Mooncake"));
    }
}
