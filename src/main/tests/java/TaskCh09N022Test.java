import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh09N022;

public class TaskCh09N022Test {
    @Test
    public void testGetFirstHalfWord() {
        Assert.assertEquals("suns", new TaskCh09N022().getFirstHalfWord("sunshine"));
    }
}
