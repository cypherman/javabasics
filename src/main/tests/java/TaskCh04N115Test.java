import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh04N115;

public class TaskCh04N115Test {
    TaskCh04N115 object = new TaskCh04N115();
    @Test
    public void testGetAnimal() {
        Assert.assertEquals("Rat", object.getAnimal(1984));
        Assert.assertEquals("Dog", object.getAnimal(1994));
    }
    @Test
    public void testGetColor() {
        Assert.assertEquals("Green", object.getColor(1984));
        Assert.assertEquals("Green", object.getColor(1994));
    }
}
