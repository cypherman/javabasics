import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh10N049;

public class TaskCh10N049Test {
    @Test
    public void testGetMaxElementIndex() {
        int[] testArray = {16, 85, 57, 2, 72, 17, 10, 87, 45, 34};
        int testIndex = testArray.length - 1;
        Assert.assertEquals(7, new TaskCh10N049().getMaxElementIndex(testArray, testIndex));
    }
}
