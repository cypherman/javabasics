import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh02N039;


public class TaskCh02N039Test {
    @Test
    public void testGetAngle() {
        Assert.assertEquals(247.75, new TaskCh02N039().getAngle(8, 15,30), 0.0001);
    }
}
