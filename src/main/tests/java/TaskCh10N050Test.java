import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh10N050;

public class TaskCh10N050Test {
    @Test
    public void testGetAckermann() {
        Assert.assertEquals(5, new TaskCh10N050().getAckermann(1, 3));
    }
}
