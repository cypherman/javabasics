import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh09N185;

public class TaskCh09N185Test {
    @Test
    public void testAreCorrectParentheses() {
        TaskCh09N185 object = new TaskCh09N185();
        String correct = "(a + b) + c";
        Assert.assertEquals("Everything is correct", object.areCorrectParentheses(correct));
        String moreLeft = "((a + b) + c";
        String exptectedLeft = "There is/are 1 extra left parentheses";
        Assert.assertEquals(exptectedLeft, object.areCorrectParentheses(moreLeft));
        String moreRight = "((a + b)) + c)";
        String exptectedRight = "There is/are extra right parentheses with the first one at position 8";
        Assert.assertEquals(exptectedRight, object.areCorrectParentheses(moreRight));
    }
}
