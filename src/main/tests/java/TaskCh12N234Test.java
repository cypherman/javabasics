import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh12N234;

public class TaskCh12N234Test {

    TaskCh12N234 object = new TaskCh12N234();
    int[][] testArray = {
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9},
            {10, 11, 12},
            {13, 14, 15}
    };
    @Test
    public void testTrimArrayRow() {
        int[][] expected = {
                {1, 2, 3},
                {4, 5, 6},
                {10, 11, 12},
                {13, 14, 15},
                {0, 0, 0}
        };
        Assert.assertArrayEquals(expected, object.trimArrayRow(testArray, 2));
    }

    @Test
    public void testTrimArrayColumn() {
        int[][] expected = {
                {1, 3, 0},
                {4, 6, 0},
                {7, 9, 0},
                {10, 12, 0},
                {13, 15, 0}
        };
        Assert.assertArrayEquals(expected, object.trimArrayColumn(testArray, 1));
    }
}
