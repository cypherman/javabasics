import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh02N043;

public class TaskCh02N043Test {
    @Test
    public void testDivide() {
        Assert.assertEquals(1, new TaskCh02N043().divide(3, 6));
    }
}
