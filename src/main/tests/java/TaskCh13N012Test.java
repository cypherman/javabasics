import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh13N012.Database;
import tasks.TaskCh13N012.Employee;

import java.util.ArrayList;
import java.util.List;

public class TaskCh13N012Test {

    Database db = new Database();

    @Test
    public void testGetYearsWorked() {
        Employee employee = new Employee("John", "Doe", "Smith", "Hawaii", 7, 2013);
        Assert.assertEquals(6, employee.getYearsWorked(7, 2019));
    }

    @Test
    public void testFindEmployeeByName() {
        List<Employee> expected = new ArrayList<>();
        expected.add(new Employee("Jack", "Nicholson", "Los Angeles", 8, 2015));
        expected.add(new Employee("Thor", "Odinson", "Asgard", 9, 1999));
        expected.add(new Employee("Thomas", "Edison", "Alva", "New Jersey", 12, 1900));
        Assert.assertArrayEquals(expected.toArray(), db.findEmployeeByName("son").toArray());
    }

    @Test
    public void testFindEmployeeByYear() {
        List<Employee> expected = new ArrayList<>();
        expected.add(new Employee("Leonardo", "DaVinci", "Rome", "Venice", 7, 1500));
        expected.add(new Employee("Sherlock", "Holmes", "London", 3, 1872));
        Assert.assertArrayEquals(expected.toArray(), db.findEmployeeByYear(125).toArray());
    }
}
