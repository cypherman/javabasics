import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh04N015;


public class TaskCh04N015Test {
    TaskCh04N015 object = new TaskCh04N015();
    @Test
    public void testCalcAge() {
        Assert.assertEquals(29, object.calcAge(12, 2014, 6, 1985));
        Assert.assertEquals(28, object.calcAge(5, 2014, 6, 1985));
        Assert.assertEquals(29, object.calcAge(6, 2014, 6, 1985));
    }

}
