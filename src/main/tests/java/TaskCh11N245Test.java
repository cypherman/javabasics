import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh11N245;

public class TaskCh11N245Test {
    @Test
    public void testReorderArray() {
        int[] testArray = {1, -1, 2, 3, -5, 4, -3};
        int[] expected = {-3, -5, - 1, 1, 2, 3, 4};
        Assert.assertArrayEquals(expected, new TaskCh11N245().reorderArray(testArray));
    }
}