import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh04N036;

public class TaskCh04N036Test {
    TaskCh04N036 object = new TaskCh04N036();
    @Test
    public void testGetLight() {
        Assert.assertEquals("red", object.getLight(3));
        Assert.assertEquals("green", object.getLight(5));
    }
}
