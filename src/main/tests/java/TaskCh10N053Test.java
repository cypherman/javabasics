import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh10N053;

public class TaskCh10N053Test {
    @Test
    public void testReverseArray() {
        int[] testArray = {1, 2, 3, 4, 5};
        int[] expected = {5, 4, 3, 2, 1};
        Assert.assertArrayEquals(expected, new TaskCh10N053().reverseArray(testArray));
    }
}
