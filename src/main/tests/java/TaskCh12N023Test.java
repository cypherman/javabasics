import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh12N023;

public class TaskCh12N023Test {

    TaskCh12N023 object = new TaskCh12N023();

    @Test
    public void testFillArrayA() {
        int[][] expected = {
                {1, 0, 0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 1, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0, 1}
        };
        Assert.assertArrayEquals(expected, object.fillArrayA(7));
    }

    @Test
    public void testFillArrayB() {
        int[][] expected = {
                {1, 0, 0, 1, 0, 0, 1},
                {0, 1, 0, 1, 0, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {1, 1, 1, 1, 1, 1, 1},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 0, 1, 0, 1, 0},
                {1, 0, 0, 1, 0, 0, 1}
        };
        Assert.assertArrayEquals(expected, object.fillArrayB(7));
    }

    @Test
    public void testFillArrayC() {
        int[][] expected = {
                {1, 1, 1, 1, 1, 1, 1},
                {0, 1, 1, 1, 1, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 1}
        };
        Assert.assertArrayEquals(expected, new TaskCh12N023().fillArrayC(7));
    }
}

