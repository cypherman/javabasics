import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh05N038;

public class TaskCh05N038Test {
    @Test
    public void testGetDistance() {
        TaskCh05N038 object = new TaskCh05N038();
        Assert.assertEquals(0.6881726, object.getDistanceFromHome() - object.getDistanceToHome(), 0.0001);
        Assert.assertEquals(5.187378, object.getDistanceFromHome() + object.getDistanceToHome(), 0.0001);
    }
}
