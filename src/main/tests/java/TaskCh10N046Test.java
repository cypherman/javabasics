import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh10N046;

public class TaskCh10N046Test {
    TaskCh10N046 object = new TaskCh10N046();

    @Test
    public void testGetElement() {
        Assert.assertEquals(10935, object.getElement(5, 3, 7));
    }

    @Test
    public void testGetSumOfElements() {
        Assert.assertEquals(16400, object.getSumOfElements(5, 3, 7));
    }
}
