import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh09N017;

public class TaskCh09N017Test {
    @Test
    public void testIsSameStartEnd() {
        Assert.assertFalse(new TaskCh09N017().isSameStartEnd("Apple"));
        Assert.assertTrue(new TaskCh09N017().isSameStartEnd("River"));
    }
}
