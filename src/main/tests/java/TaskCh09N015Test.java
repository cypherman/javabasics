import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh09N015;

public class TaskCh09N015Test {
    @Test
    public void testGetChar() {
        Assert.assertEquals('l', new TaskCh09N015().getChar("apple", 3));
    }
}
