import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh09N166;

public class TaskCh09N166Test {
    @Test
    public void testSwitchFirstWithLastWords() {
        String expected = "long! was coding all night He";
        String sentece = "He was coding all night long!";
        Assert.assertEquals(expected, new TaskCh09N166().switchFirstWithLastWords(sentece));
    }
}
