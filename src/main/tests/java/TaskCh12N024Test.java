import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh12N024;

public class TaskCh12N024Test {
    TaskCh12N024 object = new TaskCh12N024();

    @Test
    public void testFillArrayA() {
        int[][] expected = {
                {1, 1, 1, 1, 1, 1},
                {1, 2, 3, 4, 5, 6},
                {1, 3, 6, 10, 15, 21},
                {1, 4, 10, 20, 35, 56},
                {1, 5, 15, 35, 70, 126},
                {1, 6, 21, 56, 126, 252}
        };
        Assert.assertArrayEquals(expected, object.fillArrayA(6));
    }

    @Test
    public void testFillArrayB() {
        int[][] expected = {
                {1, 2, 3, 4, 5, 6},
                {2, 3, 4, 5, 6, 1},
                {3, 4, 5, 6, 1, 2},
                {4, 5, 6, 1, 2, 3},
                {5, 6, 1, 2, 3, 4},
                {6, 1, 2, 3, 4, 5}
        };
        Assert.assertArrayEquals(expected, object.fillArrayB(6));
    }
}
