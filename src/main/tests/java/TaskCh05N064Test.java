import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh05N064;

public class TaskCh05N064Test {
    @Test
    public void testGetDensity() {
        int[][] data = { {5, 7, 3, 2, 6, 33, 17, 9, 21, 50, 8, 23}, {12, 11, 4, 1, 6, 15, 9, 17, 13, 75, 16, 30} };
        Assert.assertEquals(0.8803828, new TaskCh05N064().getDensity(data), 0.0001);
    }
}
