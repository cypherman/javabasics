import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh10N042;

public class TaskCh10N042Test {
    @Test
    public void testRaiseToPower() {
        Assert.assertEquals(1024.0, new TaskCh10N042().raiseToPower(2, 10), 0.0001);
    }
}
