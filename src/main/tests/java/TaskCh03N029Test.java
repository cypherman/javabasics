import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh03N029;

public class TaskCh03N029Test {
    TaskCh03N029 object = new TaskCh03N029();
    @Test
    public void testIsOdd() {
        Assert.assertTrue(object.isOdd(5, 7));
    }
    @Test
    public void testIsLessTwenty() {
        Assert.assertFalse(object.isLessTwenty(18, 5));
    }
    @Test
    public void testIsEqualZero() {
        Assert.assertTrue(object.isEqualZero(153, 0));
    }
    @Test
    public void testAreAllNegative() {
        Assert.assertFalse(object.areAllNegative(-5, 5, 7));
    }
    @Test
    public void testIsMultipleFive() {
        Assert.assertFalse(object.isMultipleFive(5, 5, 5));
    }
    @Test
    public void testIsMoreHundred() {
        Assert.assertFalse(object.isMoreHundred(100, 3, 55));
    }
}
