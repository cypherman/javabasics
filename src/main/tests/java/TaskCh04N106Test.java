import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh04N106;

public class TaskCh04N106Test {
    TaskCh04N106 object = new TaskCh04N106();
    @Test
    public void testGetSeason() {
        Assert.assertEquals("Winter", object.getSeason(1));
        Assert.assertEquals("Spring", object.getSeason(4));
        Assert.assertEquals("Summer", object.getSeason(7));
        Assert.assertEquals("Autumn/Fall", object.getSeason(10));
        Assert.assertEquals("Error", object.getSeason(0));
    }
}
