import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh05N010;

public class TaskCh05N010Test {
    @Test
    public void testGetExchangeRateTable() {
        float[] expectedOutput = {65.78f, 131.56f, 197.34f, 263.12f, 328.9f, 394.68f, 460.46f, 526.24f,
                592.02f, 657.8f, 723.57996f, 789.36f, 855.14f, 920.92f, 986.69995f, 1052.48f, 1118.26f,1184.04f,
                1249.82f, 1315.6f};
        Assert.assertArrayEquals(new float[][]{expectedOutput}, new float[][]{new TaskCh05N010().getExchangeRateTable(65.78f)});
    }
}
