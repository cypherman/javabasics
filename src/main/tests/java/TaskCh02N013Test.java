import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh02N013;

public class TaskCh02N013Test {
    @Test
    public void testReverseNumber() {
        Assert.assertEquals(931, new TaskCh02N013().reverseNumber(139));
    }
}
