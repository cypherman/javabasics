import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh04N067;

public class TaskCh04N067Test {
    TaskCh04N067 object = new TaskCh04N067();
    @Test
    public void testGetDayOfWeek() {
        Assert.assertEquals("Workday", object.getDayOfWeek(5));
        Assert.assertEquals("Weekend", object.getDayOfWeek(7));
    }
}
