import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh12N028;

public class TaskCh12N028Test {
    @Test
    public void testGetSpiralArray() {
        int[][] expected = {
                {1, 2, 3, 4, 5},
                {16, 17, 18, 19, 6},
                {15, 24, 25, 20, 7},
                {14, 23, 22, 21, 8},
                {13, 12, 11, 10, 9}
        };
        Assert.assertArrayEquals(expected, new TaskCh12N028().getSpiralArray(5));
    }
}
