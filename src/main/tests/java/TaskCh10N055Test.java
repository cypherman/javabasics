import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh10N055;

public class TaskCh10N055Test {
    @Test
    public void testConvertNumeralSystem() {
        Assert.assertEquals("1100", new TaskCh10N055().convertNumeralSystem(12, 2));
    }
}
