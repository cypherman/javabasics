import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh10N041;

public class TaskCh10N041Test {
    @Test
    public void testGetFactorial() {
        Assert.assertEquals(3628800, new TaskCh10N041().getFactorial(10));
    }
}
