import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh10N044;

public class TaskCh10N044Test {
    @Test
    public void testGetDigitalRoot() {
        Assert.assertEquals(7, new TaskCh10N044().getDigitalRoot(389257));
    }
}
