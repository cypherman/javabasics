import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh04N033;

public class TaskCh04N033Test {
    TaskCh04N033 object = new TaskCh04N033();
    @Test
    public void testIsLastEven() {
        Assert.assertTrue(object.isLastEven(41134));
    }
    @Test
    public void tesIsLastOdd() {
        Assert.assertTrue(object.isLastOdd(42349));
    }
}
