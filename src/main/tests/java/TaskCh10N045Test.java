import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh10N045;

public class TaskCh10N045Test {
    TaskCh10N045 object = new TaskCh10N045();

    @Test
    public void testGetElement() {
        Assert.assertEquals(26, object.getElement(5, 3, 7));
    }

    @Test
    public void testGetSumOfElements() {
        Assert.assertEquals(124, object.getSumOfElements(5, 3, 7));
    }
}
