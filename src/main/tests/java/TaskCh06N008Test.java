import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh06N008;

public class TaskCh06N008Test {
    @Test
    public void testGetLessThanNum() {
        int[] expected = {1, 4, 9, 16, 25, 36, 49, 64, 81};
        Assert.assertArrayEquals(expected, new TaskCh06N008().getLessThanNum(100));
    }
}
