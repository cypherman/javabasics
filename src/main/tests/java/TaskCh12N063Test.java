import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh12N063;

public class TaskCh12N063Test {
    @Test
    public void testGetAverageStudents() {
        int[] expected = {23, 19, 20, 14, 31, 14, 16, 24, 18, 22, 12};
        int[][] testInput = {
                {19, 15, 25, 36},
                {4, 31, 8, 33},
                {39, 19, 6, 17},
                {13, 7, 24, 14},
                {31, 34, 25, 37},
                {14, 25, 7, 13},
                {18, 36, 4, 7},
                {39, 24, 26, 7},
                {25, 4, 12, 32},
                {10, 22, 27, 29},
                {10, 2, 4, 34}
        };
        Assert.assertArrayEquals(expected, new TaskCh12N063().getAverageStudents(testInput));
    }
}
