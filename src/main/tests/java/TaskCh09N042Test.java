import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh09N042;

public class TaskCh09N042Test {
    @Test
    public void testReverseWord() {
        Assert.assertEquals("ocaT", new TaskCh09N042().reverseWord("Taco"));
    }
}
