import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh10N043;

public class TaskCh10N043Test {
    TaskCh10N043 object = new TaskCh10N043();
    @Test
    public void testGetSumOfDigits() {
        Assert.assertEquals(6, object.getSumOfDigits(123));
    }

    @Test
    public void testGetQuantityOfDigits() {
        Assert.assertEquals(3, object.getQuantityOfDigits(123));
    }
}
