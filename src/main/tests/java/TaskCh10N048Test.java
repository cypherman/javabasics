import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCH10N048;

public class TaskCh10N048Test {
    @Test
    public void testGetMaxElement() {
        int[] testArray = {16, 85, 57, 2, 72, 17, 10, 87, 45, 34};
        int testLength = testArray.length;
        Assert.assertEquals(87, new TaskCH10N048().getMaxElement(testArray, testLength));
    }
}
