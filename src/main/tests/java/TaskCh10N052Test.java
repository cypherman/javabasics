import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh10N052;

public class TaskCh10N052Test {
    @Test
    public void testGetReverseNumber() {
        Assert.assertEquals(54321, new TaskCh10N052().getReverseNumber(12345));
    }
}
