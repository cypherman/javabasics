import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh11N158;

public class TaskCh11N158Test {
    @Test
    public void testGetUniqueElements() {
        int[] expected = {1, 2, 3, 5, 6, 7, 9, 0, 0, 0};
        int[] testArray = {1, 2, 3, 3, 5, 6, 7, 5, 2, 9};
        Assert.assertArrayEquals(expected, new TaskCh11N158().getUniqueElements(testArray));
    }
}
