import org.junit.Assert;
import org.junit.Test;
import tasks.TaskCh06N087.Game;

public class TaskCh06N087Test {
    Game game = new Game("Tigers", 57, "Crows", 73);
    @Test
    public void testScore() {
        String expected = "Tigers with 60 and Crows with 73 points";
        Assert.assertEquals(expected, game.score(1, 3));
        Assert.assertEquals("\nThat is not a valid team number", game.score(3, 1));
    }

    @Test
    public void testResult() {
        String expected = "Winner: Crows with 73 points\nLoser: Tigers with 57 points";
        Assert.assertEquals(expected, game.result());
        Game tieGame = new Game("Rhinos", 80, "Dolphins", 80);
        Assert.assertEquals("This game resulted in a tie with Rhinos and Dolphins both having 80 points",
                tieGame.result());
    }
}
