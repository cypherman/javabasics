package tasks;

import java.util.Scanner;

public class TaskCh10N056 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int number = in.nextInt();
        System.out.println(isPrime(number));
    }

    /**
     * Write a recursive method to check if a given number is a prime number.
     */
    public static boolean isPrime(int number) {
        return isPrimeRec(number, 2);
    }

    private static boolean isPrimeRec(int number, int divisor) {
        if (number <= 2) {
            return number == 2;
        }
        if (number % divisor == 0) {
            return false;
        }
        if (divisor * divisor > number) {
            return true;
        }
        return isPrimeRec(number, divisor + 1);
    }
}
