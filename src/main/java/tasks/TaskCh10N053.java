package tasks;

import java.util.Arrays;
import java.util.Scanner;

public class TaskCh10N053 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int length = in.nextInt();
        int[] nums = new int[length];
        for (int i = 0; i < length; i++) {
            nums[i] = in.nextInt();
        }
        System.out.println(Arrays.toString(reverseArray(nums)));
    }

    /**
     * Write a recursive method to reverse the order of the numbers in an array.
     */
    public static int[] reverseArray(int[] array) {
        int index = array.length / 2;
        int[] result = Arrays.copyOf(array, array.length);
        reverseArrayRecursion(result, index);
        return result;
    }

    private static void reverseArrayRecursion(int[] array, int index) {
        if (index != 0) {
            int temp = array[index - 1];
            array[index - 1] = array[array.length - index];
            array[array.length - index] = temp;
            reverseArrayRecursion(array, index - 1);
        }
    }
}
