package tasks;

import java.util.Scanner;

public class TaskCh10N052 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();
        System.out.println(getReverseNumber(num));
    }

    /**
     * Write a recursive method to get digits of a natural number in reverse order.
     */
    public static int getReverseNumber(int number) {
        if (number < 10) {
            return number;
        } else {
            int digit = number % 10;
            int result = digit * getPlaceValuesCount(number) + getReverseNumber(number / 10);
            return result;
        }
    }

    private static int getPlaceValuesCount(int number) {
        if (number < 10) {
            return 1;
        } else {
            int result = 10 * getPlaceValuesCount(number / 10);
            return result;
        }
    }
}
