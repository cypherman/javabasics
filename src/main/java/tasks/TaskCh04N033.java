package tasks;

import java.util.Scanner;

public class TaskCh04N033 {
    /**
     * Given a natural number,...
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int input = in.nextInt();
        System.out.println(isLastEven(input));
        System.out.println(isLastOdd(input));
    }

    /**
     * Does it end with an even digit?
     */
    public static boolean isLastEven(int input) {
        int lastDigit = input % 10;
        return lastDigit % 2 == 0;
    }

    /**
     * Does it end with an odd digit?
     */
    public static boolean isLastOdd(int input) {
        int lastDigit = input % 10;
        return lastDigit % 2 != 0;
    }
}
