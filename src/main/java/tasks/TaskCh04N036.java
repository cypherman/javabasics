package tasks;

import java.util.Scanner;

public class TaskCh04N036 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int time = in.nextInt();
        System.out.println(getLight(time));
    }

    /**
     * At the beginning of every hour traffic light turns green for 3 min
     * then turns red for 2 min and goes on like that.
     * Given time in minutes, find out which is light is on.
     */
    public static String getLight(int time) {
        return time % 5 >= 3 ? "red" : "green";
    }
}
