package tasks;

import java.util.Scanner;

public class TaskCh10N044 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int number = in.nextInt();
        System.out.println(getDigitalRoot(number));
    }

    /**
     * Calculate a digital root of a natural number recursively.
     */
    public static int getDigitalRoot(int number) {
        int root = 0;
        if (number / 10 == 0) {
            return number;
        } else {
            int digit = number % 10;
            root = digit + getDigitalRoot(number / 10);
            if (root / 10 != 0) {
                int rootDigit = root % 10;
                root = rootDigit + getDigitalRoot(root / 10);
            }
            return root;
        }
    }
}
