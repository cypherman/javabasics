package tasks;

import java.util.Scanner;

public class TaskCh10N045 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int first = in.nextInt();
        int difference = in.nextInt();
        int number = in.nextInt();
        System.out.println(getElement(first, difference, number));
        System.out.println(getSumOfElements(first, difference, number));
    }

    /**
     * Given the first element and a difference of an arithmetic sequence, recursively:
     * a) get an nth element of a sequence
     */
    public static int getElement(int firstElement, int difference, int index) {
        if (index == 0) {
            return firstElement;
        } else {
            int element = difference + getElement(firstElement, difference, index - 1);
            return element;
        }
    }

    /**
     * b) get the sum of the first n elements of a sequence
     */
    public static int getSumOfElements(int firstElement, int difference, int count) {
        if (count == 0) {
            return firstElement;
        } else {
            int element = firstElement + count * difference;
            int sum = element + getSumOfElements(firstElement, difference, count - 1);
            return sum;
        }
    }
}
