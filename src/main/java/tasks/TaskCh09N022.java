package tasks;

import java.util.Scanner;

public class TaskCh09N022 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String word = in.next();
        System.out.println(getFirstHalfWord(word));
    }

    /**
     * Given a word with an even number of letters, return its first half without any loops.
     */
    public static String getFirstHalfWord(String word) {
        String firstHalf = word.substring(0, word.length() / 2);
        return firstHalf;
    }
}
