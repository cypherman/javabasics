package tasks;

import java.util.Arrays;
import java.util.Scanner;

public class TaskCh05N010 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        float rate = in.nextFloat();
        System.out.println(Arrays.toString(getExchangeRateTable(rate)));
    }

    /**
    * Print out Dollar to Ruble conversion table (values from 1 to 20 inclusive).
    * The current exchange rate is supplied by the user input.
    */
    public static float[] getExchangeRateTable(float rate) {
        float[] table = new float[20];
        for (int i = 0; i < table.length; i++) {
            table[i] = (i + 1) * rate;
        }
        return table;
    }
}
