package tasks;

import java.util.Scanner;

public class TaskCh02N031 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int input = in.nextInt();
        System.out.println(origNumber(input));
    }

    /**
     * Given a 3 digit number, return a new number by switching the last 2 digits.
     */
    public static int origNumber(int num) {
        int firstDigit = num / 100 * 100;
        int secondDigit = num % 10 * 10;
        int thirdDigit = num / 10 % 10;
        return firstDigit + secondDigit + thirdDigit;
    }
}
