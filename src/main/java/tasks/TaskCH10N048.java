package tasks;

import java.util.Scanner;

public class TaskCH10N048 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int length = in.nextInt();
        int[] nums = new int[length];
        for (int i = 0; i < length; i++) {
            nums[i] = in.nextInt();
        }
        System.out.println(getMaxElement(nums, length));
    }

    /**
     * Write a recursive method to find max element in an array of length n.
     */
    public static int getMaxElement(int[] array, int length) {
        if (length == 1) {
            return array[0];
        } else {
            int max = array[length - 1];
            int element = getMaxElement(array, length - 1);
            if (element > max) {
                max = element;
            }
            return max;
        }
    }
}
