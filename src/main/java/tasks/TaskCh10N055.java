package tasks;

import java.util.Scanner;

public class TaskCh10N055 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int number = in.nextInt();
        int base = in.nextInt();
        System.out.println(convertNumeralSystem(number, base));
    }

    /**
     * Write a recursive method to convert a natural decimal base number to N numeral base.
     * 2 <= N <= 16
     */
    public static String convertNumeralSystem(int number, int base) {
        StringBuilder result = new StringBuilder();
        convertNumeralSystemRec(result, number, base);
        return result.toString();
    }

    private static StringBuilder convertNumeralSystemRec(StringBuilder builder, int number, int base) {
        char[] symbols = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        int remainder = number % base;
        if (number / base == 0) {
            return builder.append(symbols[remainder]);
        } else {
            convertNumeralSystemRec(builder, number / base, base);
            return builder.append(symbols[remainder]);
        }
    }
}
