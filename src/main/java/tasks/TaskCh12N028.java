package tasks;

import java.util.Arrays;
import java.util.Scanner;

public class TaskCh12N028 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();
        System.out.println(Arrays.deepToString(getSpiralArray(num)));
    }

    /**
     * Fill a 2D array as shown in the textbook (spiral).
     * Note: solve for an array of size N*N, where N % 2 == 1.
     */
    public static int[][] getSpiralArray(int size) {
        int[][] array = new int[size][size];
        int counter = 1;
        int i = 0;
        int j = 1;
        int k = 1;
        int l = 1;
        while (counter <= size * size) {
            for (int c = i; c < size - i; c++, counter++) {
                array[i][c] = counter;
            }
            i++;
            for (int c = j; c <= size - j; c++, counter++) {
                array[c][size - j] = counter;
            }
            j++;
            for (int c = size - 1 - k; c >= k - 1; c--, counter++) {
                array[size - k][c] = counter;
            }
            k++;
            for (int c = size - 1 - l; c >= l; c--, counter++) {
                array[c][l - 1] = counter;
            }
            l++;
        }
        return array;
    }
}
