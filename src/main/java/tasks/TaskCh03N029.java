package tasks;

import java.util.Scanner;

public class TaskCh03N029 {
    /**
     * Write a condition that is true when...
     * note: do not use * / + - operators
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
        int y = in.nextInt();
        int z = in.nextInt();
        System.out.println(isOdd(x, y));
        System.out.println(isLessTwenty(x, y));
        System.out.println(isEqualZero(x, y));
        System.out.println(areAllNegative(x, y, z));
        System.out.println(isMultipleFive(x, y, z));
        System.out.println(isMoreHundred(x, y, z));
    }

    /**
     * x and y are both odd numbers.
     */
    public static boolean isOdd(int x, int y) {
        boolean oddX = x % 2 != 0;
        boolean oddY = y % 2 != 0;
        return oddX && oddY;
    }

    /**
     * either x < 20 or y < 20.
     */
    public static boolean isLessTwenty(int x, int y) {
        boolean boolX = x < 20;
        boolean boolY = y < 20;
        return boolX ^ boolY;
    }

    /**
     * x or y equals to 0.
     */
    public static boolean isEqualZero(int x, int y) {
        boolean zeroX = x == 0;
        boolean zeroY = y == 0;
        return zeroX || zeroY;
    }

    /**
     * x y z are all negative numbers.
     */
    public static boolean areAllNegative(int x, int y, int z) {
        boolean negX = x < 0;
        boolean negY = y < 0;
        boolean negZ = z < 0;
        return negX && negY && negZ;
    }

    /**
     * only one of x y z is divisible by 5.
     */
    public static boolean isMultipleFive(int x, int y, int z) {
        boolean multX = x % 5 == 0;
        boolean multY = y % 5 == 0;
        boolean multZ = z % 5 == 0;
        boolean onlyX = multX && !multY && !multZ;
        boolean onlyY = multY && !multX && !multZ;
        boolean onlyZ = multZ && !multX && !multY;
        return onlyX || onlyY || onlyZ;
    }

    /**
     * at least one of x y z > 100.
     */
    public static boolean isMoreHundred(int x, int y, int z) {
        boolean boolX = x > 100;
        boolean boolY = y > 100;
        boolean boolZ = z > 100;
        return boolX || boolY || boolZ;
    }
}
