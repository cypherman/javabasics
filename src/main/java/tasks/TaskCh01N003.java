package tasks;

import java.util.Scanner;

public class TaskCh01N003 {
    /**
     * Output a number entered through keyboard.
     * The phrase "You have entered a number:" should come before the number.
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();
        System.out.print("You have entered number: " + num);
    }
}
