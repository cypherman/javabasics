package tasks;

import java.util.Scanner;

public class TaskCh09N017 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String word = in.next();
        System.out.println(isSameStartEnd(word));
    }

    /**
     * Given a word, check if it starts and ends with the same letter.
     */
    public static boolean isSameStartEnd(String word) {
        String wordLower = word.toLowerCase();
        char start = wordLower.charAt(0);
        char end = wordLower.charAt(wordLower.length() - 1);
        return start == end;
    }
}
