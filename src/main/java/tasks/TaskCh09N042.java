package tasks;

import java.util.Scanner;

public class TaskCh09N042 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String word = in.next();
        System.out.println(reverseWord(word));
    }

    /**
     * Given a word, return its reversed version using a loop.
     */
    public static String reverseWord(String word) {
        StringBuilder result = new StringBuilder();
        for (int i = word.length() - 1; i >= 0; i--) {
            result.append(word.charAt(i));
        }
        return result.toString();
    }
}
