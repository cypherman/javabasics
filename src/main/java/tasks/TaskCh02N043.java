package tasks;

import java.util.Scanner;

public class TaskCh02N043 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        System.out.println(divide(a, b));
    }

    /**
     * Given 2 ints a & b, return 1 if a is divisible by b or b is divisible by a.
     * Else, return any other number.
     * Do not use conditional operators and loops.
     */
    public static int divide(int a, int b) {
        boolean good = a % b == 0 || b % a == 0;
        boolean bad = a % b != 0 && b % a != 0;
        return Boolean.compare(good, bad);
    }
}
