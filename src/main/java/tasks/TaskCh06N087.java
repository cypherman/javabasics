package tasks;

import java.util.Scanner;

public class TaskCh06N087 {
    public static void main(String[] args) {
        Game game = new Game();
        game.play();
    }

    /**
     * Emulation of a score tracker in basketball.
     */
    public static class Game {
        private Scanner in = new Scanner(System.in);
        private String team1;
        private String team2;
        private int team1Score;
        private int team2Score;

        Game() {
            this.team1 = "";
            this.team1Score = 0;
            this.team2 = "";
            this.team2Score = 0;
        }

        public Game(String team1, int team1Score, String team2, int team2Score) {
            this.team1 = team1;
            this.team1Score = team1Score;
            this.team2 = team2;
            this.team2Score = team2Score;
        }

        void play() {
            System.out.print("Enter team #1: ");
            this.team1 = in.next();
            System.out.print("\nEnter team #2: ");
            this.team2 = in.next();
            int input = invite();
            while (input != 0) {
                System.out.print("\nEnter 1 or 2 or 3 points for a team to score: ");
                int points = in.nextInt();
                System.out.print("\n" + this.score(input, points));
                input = invite();
            }
            System.out.print("\n" + this.result());
        }

        private int invite() {
            System.out.print("\nEnter team to score (1 or 2 or 0 to finish game): ");
            return in.nextInt();
        }

        public String score(int teamNumber, int points) {
            switch (teamNumber) {
                case 1:
                    this.team1Score += points;
                    break;
                case 2:
                    this.team2Score += points;
                    break;
                default:
                    return "\nThat is not a valid team number";
            }
            String midGame = this.team1 + " with " +  this.team1Score + " and " + this.team2 + " with " + this.team2Score +
                    " points";
            return midGame;
        }

        public String result() {
            String winner = "";
            String loser = "";
            String tie;
            int winnerScore = 0;
            int loserScore = 0;
            if (this.team1Score > this.team2Score) {
                winner = this.team1;
                winnerScore = this.team1Score;
                loser = this.team2;
                loserScore = this.team2Score;
            } else if (this.team1Score < this.team2Score) {
                winner = this.team2;
                winnerScore = this.team2Score;
                loser = this.team1;
                loserScore = this.team1Score;
            } else {
                tie = "This game resulted in a tie with " + this.team1 + " and " + this.team2 + " both having " + this.team1Score + " points";
                return tie;
            }
            String endGame = "Winner: " + winner + " with " + winnerScore + " points" + "\nLoser: " + loser + " with " + loserScore +
                    " points";
            return endGame;
        }
    }
}
