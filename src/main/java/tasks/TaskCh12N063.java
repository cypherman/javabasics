package tasks;

import java.util.Arrays;
import java.util.Scanner;

public class TaskCh12N063 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[][] inputArray = new int[11][4];
        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 4; j++) {
                inputArray[i][j] = in.nextInt();
            }
        }
        System.out.println(Arrays.toString(getAverageStudents(inputArray)));
    }

    /**
     * 2D array stores info about the number of students in different school classes (from 1st to 11th).
     * Every row represents a class's year with 4 classes in every year in parallel.
     * Every cell corresponds to the number of students in that class.
     * Find average number of students in each parallel.
     */
    public static int[] getAverageStudents(int[][] array) {
        int[] result = new int[11];
        for (int i = 0; i < 11; i++) {
            int sum = 0;
            for (int j = 0; j < 4; j++) {
                sum += array[i][j];
            }
            result[i] = sum / 4;
        }
        return result;
    }
}
