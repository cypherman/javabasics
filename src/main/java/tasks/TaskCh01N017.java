package tasks;

import java.lang.Math;
import java.util.Scanner;

public class TaskCh01N017 {
    /**
     * Express mathematical operations following the rules of java.
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double inputX = in.nextDouble();
        double resultPartO = partO(inputX);
        System.out.println(resultPartO);
        double inputA = in.nextDouble();
        double inputB = in.nextDouble();
        double inputC = in.nextDouble();
        double resultPartP = partP(inputA, inputB, inputC, inputX);
        System.out.println(resultPartP);
        double resultPartR = partR(inputX);
        System.out.println(resultPartR);
        double resultPartS = partS(inputX);
        System.out.println(resultPartS);
    }

    static double partO(double x) {
        return Math.sqrt(1 - Math.pow(Math.sin(x), 2));
    }

    static double partP(double a, double b, double c, double x) {
        return 1 / Math.sqrt(a * Math.pow(x, 2) + b * x + c);
    }

    static double partR(double x) {
        return (Math.sqrt(x + 1) + Math.sqrt(x - 1)) / 2 * Math.sqrt(x);
    }

    static double partS(double x) {
        return Math.abs(x) + Math.abs(x + 1);
    }
}
