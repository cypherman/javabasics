package tasks;

import java.util.Scanner;

public class TaskCh05N064 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[][] data = new int[12][12];
        int j = 0;
        for (int i = 0; i < 12; i++) {
            data[0][i] = in.nextInt();
            while (j <= i) {
                data[1][j] = in.nextInt();
                j++;
            }
        }
        System.out.println(getDensity(data));
    }

    /**
     * There are 12 districts in a region. You know the population of each district in thousands of people and
     * the are of each district in square km.
     * Calculate the average population density of a region.
     */
    public static float getDensity(int[][] data) {
        int population = 0;
        int area = 0;
        for (int i = 0; i < 12; i++) {
            population += data[0][i];
        }
        for (int j = 0; j < 12; j++) {
            area += data[1][j];
        }
        return (float) population / area;
    }
}
