package tasks;

import java.util.Scanner;

public class TaskCh04N067 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int day = in.nextInt();
        System.out.println(getDayOfWeek(day));
    }

    /**
     * Given a day of the year 1 <= day <= 365,
     * find out if its a business day or a weekend
     */
    public static String getDayOfWeek(int day) {
        if (day % 7 < 6 && day != 7) {
            return "Workday";
        }
        return "Weekend";
    }
}
