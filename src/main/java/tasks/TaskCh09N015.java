package tasks;

import java.util.Scanner;

public class TaskCh09N015 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String word = in.next();
        int position = in.nextInt();
        System.out.println(getChar(word, position));
    }

    /**
     * Given a word and a number, return a letter of that word in that number's position.
     */
    public static char getChar(String word, int position) {
        return word.charAt(position);
    }
}
