package tasks;

import java.util.Arrays;
import java.util.Scanner;

public class TaskCh12N024 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int len = in.nextInt();
        System.out.println(Arrays.deepToString(fillArrayA(len)));
        System.out.println(Arrays.deepToString(fillArrayB(len)));
    }

    /**
     * Fill 2D arrays as shown in the textbook.
     * Note: solve for arrays of size N*N. In b) solve with one arithmetic expression for a[i][j].
     */
    public static int[][] fillArrayA(int length) {
        int[][] array = new int[length][length];
        for (int i = 0; i < length; i++) {
            if (i == 0) {
                for (int j = 0; j < length; j++) {
                    array[i][j] = 1;
                }
            } else {
                array[i][0] = 1;
                for (int j = 1; j < length; j++) {
                    array[i][j] = array[i][j - 1] + array[i - 1][j];
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayB(int length) {
        int[][] array = new int[length][length];
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                array[i][j] = (i + j) % 6 + 1;
            }
        }
        return array;
    }
}
