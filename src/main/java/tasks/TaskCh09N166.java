package tasks;

import java.util.Arrays;
import java.util.Scanner;

public class TaskCh09N166 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String sentence = in.nextLine();
        System.out.println(switchFirstWithLastWords(sentence));
    }

    /**
     * Given a sentence, switch its first word with its last one.
     * There are no trailing spaces.
     * Sentence is no longer than 10 words.
     */
    public static String switchFirstWithLastWords(String sentence) {
        String[] words = sentence.split(" ");
        String first = words[0];
        String last = words[words.length - 1];
        words[0] = last;
        words[words.length - 1] = first;
        String result = Arrays.toString(words);
        result = result.substring(1, result.length() - 1).replace(",", "");
        return result;
    }
}
