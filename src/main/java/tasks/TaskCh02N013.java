package tasks;

import java.util.Scanner;

public class TaskCh02N013 {
    /**
     * Given a 3 digit number, find a number by reading its digits from right to left.
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int input = in.nextInt();
        System.out.println(reverseNumber(input));
    }

    public static int reverseNumber(int input) {
        int firstDigit = input % 10 * 100;
        int secondDigit = input / 10 % 10 * 10;
        int thirdDigit = input / 100;
        return firstDigit + secondDigit + thirdDigit;
    }
}
