package tasks;

import java.util.Scanner;

public class TaskCh10N047 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int number = in.nextInt();
        System.out.println(getElement(number));
    }

    /**
     * Write a recursive function to calculate nth element of the Fibonacci sequence.
     */
    public static int getElement(int index) {
        if (index == 1 || index == 2) {
            return 1;
        } else {
            int element = getElement(index - 1) + getElement(index - 2);
            return element;
        }
    }
}
