package tasks;

import java.util.Arrays;
import java.util.Scanner;

public class TaskCh06N008 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();
        System.out.println(Arrays.toString(getLessThanNum(num)));
    }

    /**
     * Given a number n, from numbers 1, 4, 9, 16, 25... print out those less than n.
     */
    public static int[] getLessThanNum(int num) {
        int len = (int) Math.sqrt(num) - 1;
        int[] array = new int[len];
        for (int i = 1; i <= len; i++) {
            array[i - 1] = i * i;
        }
        return array;
    }
}
