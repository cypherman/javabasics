package tasks;

import java.util.Scanner;

public class TaskCh04N015 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int monthToday = in.nextInt();
        int yearToday = in.nextInt();
        int monthBirth = in.nextInt();
        int yearBirth = in.nextInt();
        System.out.println(calcAge(monthToday, yearToday, monthBirth, yearBirth));
    }

    /**
     * Give month and year of today's date and of birth,
     * calculate the number of full years (age).
     */
    public static int calcAge(int monthToday, int yearToday, int monthBirth, int yearBirth) {
        if (monthToday >= monthBirth) {
            return yearToday - yearBirth;
        } else {
            return yearToday - yearBirth - 1;
        }
    }
}
