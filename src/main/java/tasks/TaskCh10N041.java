package tasks;

import java.util.Scanner;

public class TaskCh10N041 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();
        System.out.println(getFactorial(num));
    }

    /**
     * Write a recursive function for getting factorial of a natural number.
     */
    public static long getFactorial(int number) {
        long factorial;
        if (number == 1) {
            return 1;
        } else {
            factorial = number * getFactorial(number - 1);
        }
        return factorial;
    }
}
