package tasks;

import java.util.Scanner;

public class TaskCh02N039 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        float hours = in.nextFloat();
        float minutes = in.nextFloat();
        float seconds = in.nextFloat();
        System.out.println(getAngle(hours, minutes, seconds));
    }

    /**
     * Given hour, minutes, and seconds, return the angle of the hour hand on the clock.
     */
    public static float getAngle(float hours, float minutes, float seconds) {
        float hoursToMin = 60 * (hours % 12);
        float secToMin = seconds / 60;
        return 0.5f * (hoursToMin + minutes + secToMin);
        //return (float) (0.5 * (60 * (hours % 12) + minutes + seconds / 60));
    }
}
