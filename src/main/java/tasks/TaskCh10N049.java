package tasks;

import java.util.Scanner;

public class TaskCh10N049 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int length = in.nextInt();
        int[] nums = new int[length];
        for (int i = 0; i < length; i++) {
            nums[i] = in.nextInt();
        }
        System.out.println(getMaxElementIndex(nums, length - 1));
    }

    /**
     * Write a recursive method to find index of the max element of an array.
     */
    public static int getMaxElementIndex(int[] array, int index) {
        if (index == 0) {
            return 0;
        } else {
            int maxIndex = index;
            int tempIndex = getMaxElementIndex(array, index - 1);
            if (array[tempIndex] > array[maxIndex]) {
                maxIndex = tempIndex;
            }
            return maxIndex;
        }
    }
}
