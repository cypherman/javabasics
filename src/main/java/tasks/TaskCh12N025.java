package tasks;

import java.util.Arrays;

public class TaskCh12N025 {
    public static void main(String[] args) {
        printArray(fillArrayA());
        printArray(fillArrayB());
        printArray(fillArrayC());
        printArray(fillArrayD());
        printArray(fillArrayE());
        printArray(fillArrayF());
        printArray(fillArrayG());
        printArray(fillArrayH());
        printArray(fillArrayI());
        printArray(fillArrayJ());
        printArray(fillArrayK());
        printArray(fillArrayL());
        printArray(fillArrayM());
        printArray(fillArrayN());
        printArray(fillArrayO());
        printArray(fillArrayP());
    }

    /**
     * Fill 2D array as shown in the textbook.
     * Note: Print arrays as a separate method. No tests required.
     */

    private static void printArray(int[][] array) {
        System.out.println(Arrays.deepToString(array));
        System.out.println();
    }

    private static int[][] fillArrayA() {
        int[][] array = new int[12][10];
        int counter = 1;
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 10; j++, counter++) {
                array[i][j] = counter;
            }
        }
        return array;
    }

    private static int[][] fillArrayB() {
        int[][] array = new int[12][10];
        int count = 1;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 12; j++, count++) {
                array[j][i] = count;
            }
        }
        return array;
    }

    private static int[][] fillArrayC() {
        int[][] array = new int[12][10];
        for (int i = 0; i < 12; i++) {
            array[i][0] = (i + 1) * 10;
            for (int j = 1; j < 10; j++) {
                array[i][j] = array[i][j - 1] - 1;
            }
        }
        return array;
    }

    private  static int[][] fillArrayD() {
        int[][] array = new int[12][10];
        int counter = 1;
        for (int i = 0; i < 10; i++) {
            for (int j = 11; j >= 0; j--, counter++) {
                array[j][i] = counter;
            }
        }
        return array;
    }

    private static int[][] fillArrayE() {
        int[][] array = new int[12][12];
        int counter = 1;
        for (int i = 0; i < 12; i++) {
            if (i % 2 == 0) {
                for (int j = 0; j < 12; j++, counter++) {
                    array[i][j] = counter;
                }
            } else {
                for (int j = 11; j >= 0; j--, counter++) {
                    array[i][j] = counter;
                }
            }
        }
        return array;
    }

    private static int[][] fillArrayF() {
        int[][] array = new int[12][10];
        int counter = 1;
        for (int i = 0; i < 10; i++) {
            if (i % 2 == 0) {
                for (int j = 0; j < 12; j++, counter++) {
                    array[j][i] = counter;
                }
            } else {
                for (int j = 11; j >= 0; j--, counter++) {
                    array[j][i] = counter;
                }
            }
        }
        return array;
    }

    private static int[][] fillArrayG() {
        int[][] array = new int[12][10];
        int counter = 1;
        for (int i = 11; i >= 0; i--) {
            for (int j = 0; j < 10; j++, counter++) {
                array[i][j] = counter;
            }
        }
        return array;
    }

    private static int[][] fillArrayH() {
        int[][] array = new int[12][10];
        int counter = 1;
        for (int i = 9; i >= 0; i--) {
            for (int j = 0; j < 12; j++, counter++) {
                array[j][i] = counter;
            }
        }
        return array;
    }

    private static int[][] fillArrayI() {
        int[][] array = new int[12][10];
        int counter = 1;
        for (int i = 11; i >= 0; i--) {
            for (int j = 9; j >= 0; j--, counter++) {
                array[i][j] = counter;
            }
        }
        return array;
    }

    private static int[][] fillArrayJ() {
        int[][] array = new int[12][10];
        int counter = 1;
        for (int i = 9; i >= 0; i--) {
            for (int j = 11; j >= 0; j--, counter++) {
                array[j][i] = counter;
            }
        }
        return array;
    }

    private static int[][] fillArrayK() {
        int[][] array = new int[12][10];
        int counter = 1;
        for (int i = 11; i >= 0; i--) {
            for (int j = 0; j < 10; j++, counter++) {
                array[i][j] = counter;
            }
            i--;
            for (int j = 9; j >= 0; j--, counter++) {
                array[i][j] = counter;
            }
        }
        return array;
    }

    private static int[][] fillArrayL() {
        int[][] array = new int[12][10];
        int counter = 1;
        for (int i = 0; i < 12; i++) {
            for (int j = 9; j >= 0; j--, counter++) {
                array[i][j] = counter;
            }
            i++;
            for (int j = 0; j < 10; j++, counter++) {
                array[i][j] = counter;
            }
        }
        return array;
    }

    private static int[][] fillArrayM() {
        int[][] array = new int[12][10];
        int counter = 1;
        for (int i = 9; i >= 0; i--) {
            for (int j = 0; j < 12; j++, counter++) {
                array[j][i] = counter;
            }
            i--;
            for (int j = 11; j >= 0; j--, counter++) {
                array[j][i] = counter;
            }
        }
        return array;
    }

    private static int[][] fillArrayN() {
        int[][] array = new int[12][10];
        int counter = 1;
        for (int i = 0; i < 10; i++) {
            for (int j = 11; j >= 0; j--, counter++) {
                array[j][i] = counter;
            }
            i++;
            for (int j = 0; j < 12; j++, counter++) {
                array[j][i] = counter;
            }
        }
        return array;
    }

    private static int[][] fillArrayO() {
        int[][] array = new int[12][10];
        int counter = 1;
        for (int i = 11; i >= 0; i--) {
            for (int j = 9; j >= 0; j--, counter++) {
                array[i][j] = counter;
            }
            i--;
            for (int j = 0; j < 10; j++, counter++) {
                array[i][j] = counter;
            }
        }
        return array;
    }

    private static int[][] fillArrayP() {
        int[][] array = new int[12][10];
        int counter = 1;
        for (int i = 9; i >= 0; i--) {
            for (int j = 11; j >= 0; j--, counter++) {
                array[j][i] = counter;
            }
            i--;
            for (int j = 0; j < 12; j++, counter++) {
                array[j][i] = counter;
            }
        }
        return array;
    }
}

