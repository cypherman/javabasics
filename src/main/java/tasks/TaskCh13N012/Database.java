package tasks.TaskCh13N012;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Database {

    private List<Employee> list = new ArrayList<>();

    public Database() {
        init();
    }

    public void add(Employee employee) {
        list.add(employee);
    }

    private void init() {
        list.add(new Employee("Zion", "Hansen", "Medley", 11, 1957));
        list.add(new Employee("Heather", "Reynolds", "Ellory", "Eggbury", 1, 1973));
        list.add(new Employee("Franklin", "Cervantes", "Caylen", "Eggrad", 3, 1984));
        list.add(new Employee("Dario", "Barron", "Norburg", 12, 1999));
        list.add(new Employee("Lucia", "Reed", "Gillian", "Tallport", 4, 1998));
        list.add(new Employee("Jack", "Nicholson", "Los Angeles", 8, 2015));
        list.add(new Employee("Thor", "Odinson", "Asgard", 9, 1999));
        list.add(new Employee("Frodo", "Baggins", "Hobbit", "Hobbiton", 8, 2017));
        list.add(new Employee("Bilbo", "Baggins", "Hobbiton", 2, 2019));
        list.add(new Employee("Martin", "King", "Luther", "New York", 5, 1983));
        list.add(new Employee("Malcolm", "X", "New York", 6, 2003));
        list.add(new Employee("Leonardo", "DaVinci", "Rome", "Venice", 7, 1500));
        list.add(new Employee("Karl", "Marx", "Germany", 10, 1902));
        list.add(new Employee("Thomas", "Edison", "Alva", "New Jersey", 12, 1900));
        list.add(new Employee("Albert", "Einstein", "New York", 1, 1903));
        list.add(new Employee("Nikola", "Tesla", "New York", 11, 1924));
        list.add(new Employee("Elon", "Musk", "Reeve", "Los Angeles", 6, 2007));
        list.add(new Employee("Sherlock", "Holmes", "London", 3, 1872));
        list.add(new Employee("Richard", "Feynman", "Phillips", "Los Angeles", 5, 1963));
        list.add(new Employee("Clinton", "Dawkins", "Richard", "Nairobi", 7, 1978));
    }

    public List<Employee> findEmployeeByName(String string) {
        List<Employee> matches = new ArrayList<>();
        for (Employee employee : list) {
            String str = string.toLowerCase();
            if (employee.getFirstName().toLowerCase().contains(str)) {
                matches.add(employee);
            } else if (employee.getLastName().toLowerCase().contains(str)) {
                matches.add(employee);
            } else if (employee.getMiddleName().toLowerCase().contains(str)) {
                matches.add(employee);
            }
        }
        return matches;
    }

    public List<Employee> findEmployeeByYear(int year) {
        List<Employee> matches = new ArrayList<>();
        LocalDate currentDate = LocalDate.now();
        int currentMonth = currentDate.getMonthValue();
        int currentYear = currentDate.getYear();
        for (Employee employee : list) {
            if (employee.getYearsWorked(currentMonth, currentYear) >= year) {
                matches.add(employee);
            }
        }
        return matches;
    }

    public void printEmployee(List<Employee> list) {
        for (Employee employee : list) {
            System.out.println(employee.getFirstName() + " " + employee.getLastName() + " " + employee.getMiddleName()
            + " " + employee.getAddress() + " Started month: " + employee.getStartMonth() + " Started year: " +
                    employee.getStartYear());
        }
    }
}
