package tasks.TaskCh13N012;

import java.util.List;

public class TaskCh13N012 {
    /**
     * Given info about N number of employees, return the number of years an employee has worked in a company.
     * Employee info: first name, last name, middle name, address, and starting work date (month and year).
     * Write a method that will return a number of years worked given month and year.
     * Implement a method that will search by String (case insensitive) even partial matches with employee names.
     * For a search: implement class Database that encapsulate storage of all employees and search methods.
     * For employees: implement class Employee with according constructors.
     */
    public static void main(String[] args) {
        Database db = new Database();
        Employee employee = new Employee("John", "Doe", "Smith", "Hawaii", 7, 2013);
        System.out.println(employee.getYearsWorked(7, 2019));
        System.out.println();
        System.out.println("Employees with 'ins' in their name");
        List<Employee> list = db.findEmployeeByName("ins");
        db.printEmployee(list);
        System.out.println();
        System.out.println("Worked more than 50 years:");
        list.clear();
        list = db.findEmployeeByYear(50);
        db.printEmployee(list);
    }
}
