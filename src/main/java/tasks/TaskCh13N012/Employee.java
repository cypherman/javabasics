package tasks.TaskCh13N012;

import java.util.Objects;

public class Employee {

    private String firstName;
    private String lastName;
    private String middleName;
    private String address;
    private int startMonth;
    private int startYear;

    public Employee(String firstName, String lastName, String address, int startMonth, int startYear) {
        this(firstName, lastName, "", address, startMonth, startYear);
    }

    public Employee(String firstName, String lastName, String middleName, String address, int startMonth, int startYear) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.address = address;
        this.startMonth = startMonth;
        this.startYear = startYear;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return startMonth == employee.startMonth &&
                startYear == employee.startYear &&
                Objects.equals(firstName, employee.firstName) &&
                Objects.equals(lastName, employee.lastName) &&
                Objects.equals(middleName, employee.middleName) &&
                Objects.equals(address, employee.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, middleName, address, startMonth, startYear);
    }

    public int getYearsWorked(int month, int year) {
        int years = year - getStartYear();
        if (month < getStartMonth()) {
            years -= 1;
        }
        return years;
    }
}
