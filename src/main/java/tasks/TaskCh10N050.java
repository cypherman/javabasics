package tasks;

import java.math.BigInteger;
import java.util.Scanner;

public class TaskCh10N050 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        long firstArg = in.nextLong();
        long secondArg = in.nextLong();
        System.out.println(getAckermann(firstArg, secondArg));
    }

    /**
     * Implement a recursive method to compute values of the Ackermann function with non-negative integers.
     */
    public static long getAckermann(long firstNum, long secondNum) {
        long result;
        if (firstNum == 0) {
            result = secondNum + 1;
        } else if (firstNum != 0 && secondNum == 0) {
            result = getAckermann(firstNum - 1, 1);
        } else {
            result = getAckermann(firstNum - 1, getAckermann(firstNum, secondNum - 1));
        }
        return result;
    }
}
