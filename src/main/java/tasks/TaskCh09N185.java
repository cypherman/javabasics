package tasks;

import java.util.Scanner;

public class TaskCh09N185 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String expression = in.nextLine();
        System.out.println(areCorrectParentheses(expression));
    }

    /**
     * Line contains an arithmetic expression with parentheses
     * (including) parentheses inside of other parentheses.
     * Check if parentheses are placed correctly (aka no extra ones).
     * In case of extra right parentheses, return an error with the position of the first occurrence.
     * In case of extra left parentheses, return a message with the quantity of such parentheses.
     * In case everything is correct, return a message stating that.
     */
    public static String areCorrectParentheses(String expression) {
        int leftParenthCounter = 0;
        int rightParenthCounter = 0;
        String result = "Everything is correct";
        String string = expression.replaceAll(" ", "");
        for (char symbol : expression.toCharArray()) {
            if (symbol == '(') {
                leftParenthCounter++;
            } else if (symbol == ')') {
                rightParenthCounter++;
            }
        }
        if (leftParenthCounter > rightParenthCounter) {
            int extraLeftParenth = leftParenthCounter - rightParenthCounter;
            result = "There is/are " + extraLeftParenth + " extra left parentheses";
        } else if (leftParenthCounter < rightParenthCounter) {
            int rightParenthIndex = expression.indexOf(')') + 1;
            result = "There is/are extra right parentheses with the first one at position " + rightParenthIndex;
        }
        return result;
    }
}
