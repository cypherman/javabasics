package tasks;

import java.util.Arrays;
import java.util.Scanner;

public class TaskCh12N023 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int len = in.nextInt();
        System.out.println(Arrays.deepToString(fillArrayA(len)));
        System.out.println(Arrays.deepToString(fillArrayB(len)));
        System.out.println(Arrays.deepToString(fillArrayC(len)));
    }

    /**
     * Fill a 2D array according to the textbook.
     * Note: solve for arrays of size N*N.
     */

    public static int[][] fillArrayA(int length) {
        int[][] array = new int[length][length];
        for (int i = 0; i < length; i++) {
            array[i][i] = 1;
            array[i][length - 1 - i] = 1;
        }
        return array;
    }

    public static int[][] fillArrayB(int length) {
        int[][] array = new int[length][length];
        for (int i = 0; i < length; i++) {
            array[i][length / 2] = 1;
            if (i == length - 1 - i) {
                for (int j = 0; j < length; j++) {
                    array[i][j] = 1;
                }
            }
            array[i][i] = 1;
            array[i][length - 1 - i] = 1;
        }
        return array;
    }

    public static int[][] fillArrayC(int length) {
        int[][] array = new int[length][length];
        int mid = length / 2;
        for (int i = 0; i < length; i++) {
            if (i == 0 || i == length - 1) {
                for (int j = 0; j < length; j++) {
                    array[i][j] = 1;
                }
            } else if (i < mid) {
                for (int j = i; j < length - i; j++) {
                    array[i][j] = 1;
                }
            } else if (i == mid) {
                array[i][i] = 1;
            } else {
                for (int j = length - i - 1; j <= i; j++) {
                    array[i][j] = 1;
                }
            }
        }
        return array;
    }
}

