package tasks;

import java.util.Scanner;

public class TaskCh10N042 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double num = in.nextDouble();
        int pow = in.nextInt();
        System.out.println(raiseToPower(num, pow));
    }

    /**
     * Write a recursive function for raising a real number to some power.
     * Power is any natural number.
     */
    public static double raiseToPower(double number, int power) {
        double result;
        if (power == 1) {
            return number;
        } else if (power == 0) {
            return 1;
        } else {
            result = number * raiseToPower(number, power - 1);
            return result;
        }
    }
}
