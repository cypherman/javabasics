package tasks;

import java.util.Arrays;
import java.util.Scanner;

public class TaskCh12N234 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int rows = in.nextInt();
        int columns = in.nextInt();
        int[][] inputArray = new int[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                inputArray[i][j] = in.nextInt();
            }
        }
        System.out.println("Enter row index: ");
        int row = in.nextInt();
        System.out.println(Arrays.deepToString(trimArrayRow(inputArray, row)));
        System.out.println("Enter column index: ");
        int column = in.nextInt();
        System.out.println(Arrays.deepToString(trimArrayColumn(inputArray, column)));
    }

    /**
     * Give a 2D array:
     * a) remove its r's row
     * b) remove its c's column
     * Note: removing a row means moving all its following rows up by 1 and assigning the last row all zeroes.
     * And removing a column means moving all its following columns left by 1 and assigning zeroes to the last one.
     */
    public static int[][] trimArrayRow(int[][] array, int rowIndex) {
        int[][] result = new int[array.length][];
        for (int i = 0; i < array.length; i++) {
            result[i] = new int[array[i].length];
            System.arraycopy(array[i], 0, result[i], 0, array[i].length);
        }
        for (int i = rowIndex; i < result.length - 1; i++) {
            for (int j = 0; j < result[i].length; j++) {
                result[i][j] = result[i + 1][j];
            }
        }
        for (int j = 0; j < result[result.length - 1].length; j++) {
            result[result.length - 1][j] = 0;
        }
        return result;
    }

    public static int[][] trimArrayColumn(int[][] array, int columnIndex) {
        int[][] result = new int[array.length][];
        for (int i = 0; i < array.length; i++) {
            result[i] = new int[array[i].length];
            System.arraycopy(array[i], 0, result[i], 0, array[i].length);
        }
        for (int i = columnIndex; i < result[i].length - 1; i++) {
            for (int j = 0; j < result.length; j++) {
                result[j][i] = result[j][i + 1];
            }
        }
        for (int i = 0; i < result.length; i++) {
            result[i][result[i].length - 1] = 0;
        }
        return result;
    }
}
