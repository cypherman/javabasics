package tasks;

import java.util.Scanner;

public class TaskCh04N115 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int year = in.nextInt();
        String result = getAnimal(year) + ", " + getColor(year);
        System.out.println(result);
    }

    /**
     * Given a year, calculate its corresponding color
     * and animal according to Chinese zodiac calendar.
     */
    public static String getAnimal(int year) {
        String animal;
        int zodiacYear = Math.abs((year - 4)) % 12;
        switch (zodiacYear) {
            case 0:
                animal = "Rat";
                break;
            case 1:
                animal = "Cow";
                break;
            case 2:
                animal = "Tiger";
                break;
            case 3:
                animal = "Hare";
                break;
            case 4:
                animal = "Dragon";
                break;
            case 5:
                animal = "Snake";
                break;
            case 6:
                animal = "Horse";
                break;
            case 7:
                animal = "Sheep";
                break;
            case 8:
                animal = "Monkey";
                break;
            case 9:
                animal = "Rooster";
                break;
            case 10:
                animal = "Dog";
                break;
            case 11:
                animal = "Pig";
                break;
            default:
                System.out.println("Wrong year");
                return "Error";
        }
        return animal;
    }

    public static String getColor(int year) {
        String color;
        int zodiacColor = Math.abs((year - 4)) % 10 / 2;
        switch (zodiacColor) {
            case 0:
                color = "Green";
                break;
            case 1:
                color = "Red";
                break;
            case 2:
                color = "Yellow";
                break;
            case 3:
                color = "White";
                break;
            case 4:
                color = "Black";
                 break;
            default:
                System.out.println("Wrong year");
                return "Error";
        }
        return color;
    }
}
