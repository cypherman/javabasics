package tasks;

import java.util.Scanner;

public class TaskCh10N046 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int first = in.nextInt();
        int ratio = in.nextInt();
        int num = in.nextInt();
        System.out.println(getElement(first, ratio, num));
        System.out.println(getSumOfElements(first, ratio, num));
    }

    /**
    * Given the first element and a common ratio of an geometric sequence, recursively:
    * a) get an nth element of a sequence
    */
    public static int getElement(int firstElement, int ratio, int index) {
        if (index == 0) {
            return firstElement;
        } else {
            int element = ratio * getElement(firstElement, ratio, index - 1);
            return element;
        }
    }

    /**
     * b) get the sum of the first n elements of a sequence
     */
    public static int getSumOfElements(int firstElement, int ratio, int count) {
        if (count == 0) {
            return firstElement;
        } else {
            int element = (int) (firstElement * Math.pow(ratio, count));
            int sum = element + getSumOfElements(firstElement, ratio, count - 1);
            return sum;
        }
    }
}
