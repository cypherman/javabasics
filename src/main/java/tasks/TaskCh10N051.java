package tasks;

public class TaskCh10N051 {
    public static void main(String[] args) {
        int num = 5;
        System.out.println("The first output:");
        recMethodA(num);
        System.out.println("\nThe second output");
        recMethodB(num);
        System.out.println("\nThe third output");
        recMethodC(num);
    }

    /**
     * Implementation of recursive methods from the textbook.
     */
    private static void recMethodA(int number) {
        if (number > 0) {
            System.out.print(number + " ");
            recMethodA(number - 1);
        }
    }

    private static void recMethodB(int number) {
        if (number > 0) {
            recMethodB(number - 1);
            System.out.print(number + " ");
        }
    }

    private static void recMethodC(int number) {
        if (number > 0) {
            System.out.print(number + " ");
            recMethodC(number - 1);
            System.out.print(number + " ");
        }
    }
}
