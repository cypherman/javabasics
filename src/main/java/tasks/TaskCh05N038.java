package tasks;

public class TaskCh05N038 {
    public static void main(String[] args) {
        float distFromHome = getDistanceFromHome() - getDistanceToHome();
        float totalDistance = getDistanceFromHome() + getDistanceToHome();
        System.out.println(distFromHome);
        System.out.println(totalDistance);
    }

    /**
     * A man travels a distance of 1/steps for n steps and changing his direction to the opposite before each step
     * after the first one.
     * Calculate distance from starting point after 100's step.
     * Calculate total distance traveled after 100's step.
     */
    public static float getDistanceFromHome() {
        float distFromHome = 0;
        for (int i = 1; i <= 100; i += 2) distFromHome += 1f / i;
        return distFromHome;
    }

    public static float getDistanceToHome() {
        float distToHome = 0;
        for (int i = 2; i <= 100; i += 2) distToHome += 1f / i;
        return distToHome;
    }
}
