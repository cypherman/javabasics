package tasks;

import java.util.Scanner;

public class TaskCh09N107 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String word = in.next();
        System.out.println(switchAWithO(word));
    }

    /**
     * Given a word, replace the first appearance of 'a' with the last of 'o'.
     * Note that those letters might not be present in a given word.
     */
    public static String switchAWithO(String word) {
        boolean hasA = word.toLowerCase().contains("a");
        boolean hasO = word.toLowerCase().contains("o");
        if (hasA && hasO) {
            int first = word.toLowerCase().indexOf("a");
            int last = word.toLowerCase().lastIndexOf("o");
            char[] chars = word.toCharArray();
            for (int i = 0; i < chars.length; i++) {
                if (i == first) {
                    chars[i] = 'o';
                } else if (i == last) {
                    chars[i] = 'a';
                }
            }
            word = new String(chars);
        }
        return word;
    }
}
