package tasks;

import java.util.Arrays;
import java.util.Scanner;

public class TaskCh11N158 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int len = in.nextInt();
        int[] nums = new int[len];
        for (int i = 0; i < len; i++) {
            nums[i] = in.nextInt();
        }
        System.out.println(Arrays.toString(getUniqueElements(nums)));
    }

    /**
     * Remove all duplicate elements from an array, thus leaving only the first unique ones.
     * Removing means: moving all elements to the left and assigning 0 to the last position.
     */
    public static int[] getUniqueElements(int[] array) {
        int[] result = new int[array.length];
        System.arraycopy(array, 0, result, 0, array.length);
        int pos = 0;
        while (pos < result.length - 1) {
            int temp = result[pos];
            if (temp == 0) {
                break;
            }
            for (int i = pos + 1; i < result.length - 1; i++) {
                if (temp == result[i]) {
                    for (int j = i; j < result.length - 1; j++) {
                        if (result[j] == 0) {
                            break;
                        }
                        result[j] = result[j + 1];
                    }
                    result[result.length - 1] = 0;
                }
            }
            pos++;
        }
        return result;
    }
}
