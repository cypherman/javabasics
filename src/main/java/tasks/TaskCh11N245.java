package tasks;

import java.util.Arrays;
import java.util.Scanner;

public class TaskCh11N245 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int len = in.nextInt();
        int[] nums = new int[len];
        for (int i = 0; i < len; i++) {
            nums[i] = in.nextInt();
        }
        System.out.println(Arrays.toString(reorderArray(nums)));
    }

    /**
     * Given an array, rewrite all of its elements to an array of the same size in the following order:
     * all negative elements first, then everything else.
     * Note: use only one pass through the given array.
     */
    public static int[] reorderArray(int[] array) {
        int[] result = new int[array.length];
        int counter = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < 0) {
                for (int j = array.length - 1; j > 0; j--) {
                    result[j] = result[j - 1];
                }
                result[0] = array[i];
            } else {
                result[counter] = array[i];
            }
            counter++;
        }
        return result;
    }
}
