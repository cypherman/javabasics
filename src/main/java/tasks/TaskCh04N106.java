package tasks;

import java.util.Scanner;

public class TaskCh04N106 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int input = in.nextInt();
        System.out.println(getSeason(input));
    }

    /**
     * Given a month's number, establish which season it is a part of.
     * Solve with switch
     */
    public static String getSeason(int numMonth) {
        switch (numMonth) {
            case 1:
            case 2:
            case 12:
                return "Winter";
            case 3:
            case 4:
            case 5:
                return "Spring";
            case 6:
            case 7:
            case 8:
                return "Summer";
            case 9:
            case 10:
            case 11:
                return "Autumn/Fall";
            default:
                System.out.println("There is no such month");
                return "Error";
        }
    }
}
