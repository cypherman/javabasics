package tasks;

import java.util.Scanner;

public class TaskCh10N043 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int number = in.nextInt();
        System.out.println(getSumOfDigits(number));
        System.out.println(getQuantityOfDigits(number));
    }

    /**
     * Given a natural number, write a recursive method to:
     * a) calculate the sum of digits of that number
     */
    public static int getSumOfDigits(int number) {
        int sum = 0;
        if (number / 10 == 0) {
            return number;
        } else {
            int digit = number % 10;
            sum = digit + getSumOfDigits(number / 10);
            return sum;
        }
    }

    /**
     * b) calculate the number of digits
     */
    public static int getQuantityOfDigits(int number) {
        int quantity = 0;
        if (number / 10 == 0) {
            return 1;
        } else {
            quantity = 1 + getQuantityOfDigits(number / 10);
            return quantity;
        }
    }
}
